//
//  ViewController.swift
//  InitWebService
//
//  Created by AlexM on 17/02/15.
//  Copyright (c) 2015 AMO. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UIPickerViewDelegate {
    @IBOutlet weak var celsiusText: UITextField!
    @IBOutlet weak var kelvinText: UITextField!
    @IBOutlet weak var humidityText: UITextField!
    @IBOutlet weak var windText: UITextField!
    @IBOutlet weak var citiesPicker: UIPickerView!
    @IBOutlet weak var infoCityText: UILabel!
    
    var citiesArray = ["Barcelona", "Lleida", "Girona", "Tarragona"]
    var city = ""

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    @IBAction func acceptAction(sender: AnyObject) {
        infoCityText.text = "Información sobre \(city)"
        
        //Primero creamos la configuración de la sesión
        var sessionConfig: NSURLSessionConfiguration = NSURLSessionConfiguration.defaultSessionConfiguration()
        sessionConfig.allowsCellularAccess = false
        
        //Solo aceptamos respuestas en JSON
        sessionConfig.HTTPAdditionalHeaders = ["Accept":"application/json"]
        
        //Configuramos timeouts y conexiones permitidas
        sessionConfig.timeoutIntervalForRequest = 30.0
        sessionConfig.timeoutIntervalForResource = 60.0
        sessionConfig.HTTPMaximumConnectionsPerHost = 1
        
        //Creamos la sesión con la configuración anterior y guardo los datos en un NSDictionary
        var session = NSURLSession(configuration: sessionConfig)
        var url = "http://api.openweathermap.org/data/2.5/weather?q=\(city),es"

        session.dataTaskWithURL(NSURL(string: url)!, completionHandler: {(data, response, error) in
            var dic: NSDictionary = NSJSONSerialization.JSONObjectWithData(data, options:NSJSONReadingOptions(0), error: nil) as! NSDictionary
            
            //Imprimo el diccionario para ver sus datos
            var humedad: AnyObject! = (dic["main"] as! NSDictionary)["humidity"]
            var viento: AnyObject! = (dic["wind"] as! NSDictionary)["speed"]
            var kelvin: AnyObject! = (dic["main"] as! NSDictionary)["temp"]
            var celsius = kelvin as! Float - 274.15 as Float
            
            dispatch_async(dispatch_get_main_queue(), {() in
                //Aquí debes guardar las varibles humedad, kelvin y celsius en los IBOutlets
                self.celsiusText.text = "\(celsius)"
                self.kelvinText.text = "\(kelvin)"
                self.humidityText.text = "\(humedad)"
                self.windText.text = "\(viento)"
            })
            
            //Calculamos la temperatura en celsius
            println(dic)
        }).resume()
    }
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView!) -> Int {
    //Devuelve el número de componentes (columnas) que tendrá elpickerView. En este caso, 1.
        return 1;
    }
    
    func pickerView(pickerView: UIPickerView!, numberOfRowsInComponent component: Int) -> Int {
        //Devuelve el número de filas que tendrá el pickerView.
        return citiesArray.count
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String! {
        //Devuelve el título de cada fila del pickerView (el textoque se mostrará)
        return citiesArray[row]
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int){
        //Acción que se ejecutará cuando se seleccione una fila del pickerView
        city = citiesArray[row]
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

